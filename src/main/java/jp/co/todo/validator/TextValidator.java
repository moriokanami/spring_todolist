package jp.co.todo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import jp.co.todo.validatora.nnotation.Text;

public class TextValidator implements ConstraintValidator<Text, String> {

    public void initialize(Text text) {
    }

	public boolean isValid(String input, ConstraintValidatorContext con) {
	if (input == null) {
		return false;
	}
	if (input.matches("( |　)+")) {
		return false;
	}
	if (input.length() == 0) {
		return false;
	}
	return true;
	}
}
