package jp.co.todo.controller;

import jp.co.todo.dto.task.TaskCompDto;
import jp.co.todo.dto.task.TaskDto;
import jp.co.todo.form.TaskForm;
import jp.co.todo.form.CheckForm;
import jp.co.todo.service.TaskService;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TaskController {

	@Autowired
	private TaskService taskService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String getHome(Model model) {

		TaskForm form = new TaskForm();
		form.setText(null);

		CheckForm chForm = new CheckForm();

		List<TaskDto> todolist = taskService.getToDoList();

		model.addAttribute("todolist", todolist);
		model.addAttribute("taskForm", form);
		model.addAttribute("checkForm", chForm);
		return "home";
	}

	@RequestMapping(value = "/home", method = RequestMethod.POST, params = "insertTask")
	public String insertTask(@Valid @ModelAttribute TaskForm form, BindingResult result, Model model) {

		if(result.hasErrors()) {

			List<TaskDto> todolist = taskService.getToDoList();

			CheckForm chForm = new CheckForm();

			model.addAttribute("todolist", todolist);
			model.addAttribute("checkForm", chForm);
			model.addAttribute("taskForm", form);
			model.addAttribute("message", "以下のエラーを解消してください");
			return "home";
		} else {
			TaskDto dto = new TaskDto();
			BeanUtils.copyProperties(form, dto);
			int count = taskService.insertTask(dto);
			Logger.getLogger(TaskController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		}
		return "redirect:/home/";
	}

	@RequestMapping(value = "/home", method = RequestMethod.POST, params = "checkTask")
	public String taskCompleted(@ModelAttribute CheckForm chForm, Model model) {

		TaskDto dto = new TaskDto();
		BeanUtils.copyProperties(chForm, dto);
		int count = taskService.checkTask(dto);
		Logger.getLogger(TaskController.class).log(Level.INFO, "更新件数は" + count + "件です。");
		return "redirect:/home/";
	}

	@RequestMapping(value = "/home/edit", method = RequestMethod.GET)
    public String editer(Model model) {

		List<TaskDto> todolist = taskService.getToDoList();
		CheckForm chForm = new CheckForm();
		TaskForm form = new TaskForm();
		form.setText(null);

		model.addAttribute("todolist", todolist);
		model.addAttribute("editForm", form);
		model.addAttribute("deleteForm", form);
		model.addAttribute("checkForm", chForm);
		return "editer";
	}

	@RequestMapping(value = "/home/edit", method = RequestMethod.POST, params="updateTask")
	public String updateTask(@Valid @ModelAttribute TaskForm form, BindingResult result, Model model) {

		if(result.hasErrors()) {
			List<TaskDto> todolist = taskService.getToDoList();
			CheckForm chForm = new CheckForm();
			form.setText(null);

			model.addAttribute("todolist", todolist);
			model.addAttribute("editForm", form);
			model.addAttribute("deleteForm", form);
			model.addAttribute("checkForm", chForm);

			model.addAttribute("message", "タスクは30文字以内で記入してください。空欄の場合は登録できません。");
			return "editer";

		} else {
			TaskDto dto = new TaskDto();
			BeanUtils.copyProperties(form, dto);
			int count = taskService.updateTask(dto);
			Logger.getLogger(TaskController.class).log(Level.INFO, "更新件数は" + count + "件です。");
		}
		return "redirect:/home/edit";
	}

	@RequestMapping(value = "/home/edit", method = RequestMethod.POST, params = "deleteTask")
	public String deleteTask(@ModelAttribute TaskForm form, Model model) {

		TaskDto dto = new TaskDto();
		BeanUtils.copyProperties(form, dto);
		int count = taskService.deleteTask(dto);
		Logger.getLogger(TaskController.class).log(Level.INFO, "削除件数は" + count + "件です。");
		return "redirect:/home/edit";
	}

	@RequestMapping(value = "/home/edit", method = RequestMethod.POST, params = "checkTask")
	public String editTaskCompleted(@ModelAttribute CheckForm chForm, Model model) {

		TaskDto dto = new TaskDto();
		BeanUtils.copyProperties(chForm, dto);
		int count = taskService.checkTask(dto);
		Logger.getLogger(TaskController.class).log(Level.INFO, "更新件数は" + count + "件です。");
		return "redirect:/home/edit";
	}
}