package jp.co.todo.validatora.nnotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import jp.co.todo.validator.TextValidator;

@Documented
@Constraint(validatedBy = TextValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Text {
    String message() default "Please input a memo.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}