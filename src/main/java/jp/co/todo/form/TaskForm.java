package jp.co.todo.form;

import java.sql.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import jp.co.todo.validatora.nnotation.Text;

public class TaskForm {

	@Text
	@Size(max = 30)
	private String text;
	private int id;
	private int taskCompleted;
	private Date createdDate;
	private Date updatedDate;

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTaskCompleted() {
		return taskCompleted;
	}
	public void setTaskCompleted(int taskCompleted) {
		this.taskCompleted = taskCompleted;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
