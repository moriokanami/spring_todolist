package jp.co.todo.mapper;

import java.util.List;

import jp.co.todo.dto.task.TaskDto;
import jp.co.todo.entity.Task;
import jp.co.todo.entity.TaskComp;

public interface TaskMapper {

	Task getTask(int id);

	int insertTask(TaskDto dto);

	List<Task> getToDoList();
	List<TaskComp> getTaskComp();

	int updateTask(TaskDto dto);
	int checkTask(TaskDto dto);
	int deleteTask(TaskDto dto);
}
