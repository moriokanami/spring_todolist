package jp.co.todo.service;

import jp.co.todo.dto.task.TaskCompDto;
import jp.co.todo.dto.task.TaskDto;
import jp.co.todo.entity.Task;
import jp.co.todo.entity.TaskComp;
import jp.co.todo.mapper.TaskMapper;

import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

    @Autowired
    private TaskMapper taskMapper;

    public TaskDto getTask(Integer id) {
        TaskDto dto = new TaskDto();
        Task entity = taskMapper.getTask(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public int insertTask(TaskDto dto) {
    	int count = taskMapper.insertTask(dto);
    	return count;
    }

    public List<TaskDto> getToDoList() {
    	List<Task> taskList = taskMapper.getToDoList();
    	List<TaskDto> resultList = convertToDto(taskList);
    	return resultList;
    }

    private List<TaskDto> convertToDto(List<Task> taskList) {
    	List<TaskDto> resultList = new LinkedList<TaskDto>();
		for (Task entity : taskList) {
			TaskDto dto = new TaskDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

    public int checkTask(TaskDto dto) {
    	int count = taskMapper.checkTask(dto);
    	return count;
    }

    public int updateTask(TaskDto dto) {
    	int count = taskMapper.updateTask(dto);
    	return count;
    }

    public int deleteTask(TaskDto dto) {
    	int count = taskMapper.deleteTask(dto);
    	return count;
    }

}