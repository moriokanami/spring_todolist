package jp.co.todo.dto.task;

import java.sql.Date;

public class TaskDto {

	private Integer id;
	private String text;
	private int taskCompleted;
	private Date createdDate;
	private Date updatedDate;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getTaskCompleted() {
		return taskCompleted;
	}
	public void setTaskCompleted(int taskCompleted) {
		this.taskCompleted = taskCompleted;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
