<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>TODOリスト</title>
		<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">
		<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
	</head>
	<body>
		<h1>TODOリスト</h1>
		<div id="header">
			<div id="nav">
				<ul>
					<li><a href="http://localhost:8080/ToDoList/home/" title="ホーム"><i class="fas fa-home fa-2x"></i></a></li>
					<li><a href="http://localhost:8080/ToDoList/home/edit" title="編集"><i class="fas fa-edit fa-2x"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="errorMessage">
			<c:out value="${message}" />
		</div>
		<div class="insert-task-form">
			<form:form modelAttribute="taskForm">
				<div class="errorMessage"><form:errors path="*" /></div>
						<i class="fas fa-plus"></i>
						<form:input path="text" size="30"/>
						<form:button name="insertTask"><i class="fas fa-pencil-alt fa-2x"></i></form:button>
			</form:form>
		</div>
		<table class="todolist">
			<tr>
				<th><i class="far fa-clock fa-2x"></i></th>
				<th><i class="fas fa-tasks fa-2x"></i></th>
				<th><i class="far fa-check-square fa-2x"></i></th>
			</tr>

			<c:forEach items="${todolist}" var="task">
				<tr>
					<td><fmt:formatDate value="${task.createdDate}" pattern="MM/dd" /></td>
					<td><c:out value="${task.text}"></c:out></td>
					<td><c:if test="${task.taskCompleted == 0}">
							<form:form modelAttribute="checkForm">
								<form:input type="hidden" path="id" value="${task.id}"/>
								<form:input type="hidden" path="taskCompleted" value="1"/>
								<form:button name="checkTask" class="button"><i class="fas fa-check fa-2x faa-wrench"></i></form:button>
							</form:form>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>