<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>編集</title>
		<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>
		<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
	</head>
	<body>
		<h1>TODOリスト</h1>

		<div id="header">
			<div id="nav">
				<ul>
					<li><a href="http://localhost:8080/ToDoList/home/" title="ホーム"><i class="fas fa-home fa-2x"></i></a></li>
					<li><a href="http://localhost:8080/ToDoList/home/edit" title="編集"><i class="fas fa-edit fa-2x"></i></a></li>
				</ul>			</div>
		</div>
		<p>
			<c:out value="${message}" />
		</p>

		<div class="todolist-edit-wrapper">
			<c:forEach items="${todolist}" var="task">
				<div class="todolist-edit">
					<form:form modelAttribute="editForm">
						<div><form:errors path="*" /></div>

						<ul>
							<li><fmt:formatDate value="${task.createdDate}" pattern="MM/dd" /></li>
							<li><form:input path="text" value="${task.text}" size="20" /></li>
							<li><form:input type="hidden" path="id" value="${task.id}"/>
								<form:button name="updateTask"><i class="fas fa-pencil-alt fa-2x"></i></form:button></li>
						</ul>
					</form:form>
					<form:form modelAttribute="deleteForm">
						<form:input type="hidden" path="id" value="${task.id}"/>
						<form:button name="deleteTask" class="edit-button" id="delete-button"><i class="far fa-times-circle fa-2x"></i></form:button>
					</form:form>

					<c:if test="${task.taskCompleted == 1}">
						<form:form modelAttribute="checkForm">
							<form:input type="hidden" path="id" value="${task.id}"/>
							<form:input type="hidden" path="taskCompleted" value="0"/>
							<form:button name="checkTask" class="edit-button"><i class="fas fa-check fa-2x"></i></form:button>
						</form:form>
					</c:if>
					<c:if test="${task.taskCompleted == 0}">
						<form:form modelAttribute="checkForm">
							<form:input type="hidden" path="id" value="${task.id}"/>
							<form:input type="hidden" path="taskCompleted" value="1"/>
							<form:button name="checkTask" class="edit-button"><i class="far fa-square fa-2x"></i></form:button>
						</form:form>
					</c:if>
				</div>
			</c:forEach>
		</div>
	</body>
</html>